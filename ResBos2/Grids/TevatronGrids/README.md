## Organization

This directory contains the grids for the Tevatron, where each individual folder labels the type of grids contained within. The options for the grids are:

1. The PDF set used
2. The scale for the calculation
3. Whether it is a resummed grid (w321/w432) or y grid (NLO/NNLO)
4. The non-perturbative function used

## Credits
The Tevatron grid files supplied in this folder are from the High Energy Physics group of the University of Science and Technology of China (USTC), Hefei, China
