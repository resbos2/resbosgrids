This set of grid files are for Tevatron Run-2 with the following 
sets of g1, g2 and  g3 values. 

      g1=(0.21)
      g2=(0.46 0.48 0.50 0.52 0.54 0.56 0.58 0.60 0.62 
          0.64 0.66 0.68 0.70 0.72 0.74 0.76 0.78 0.80 0.82 0.84)
      g3=(-0.6);
 
